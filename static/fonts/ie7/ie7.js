/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icons\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-expert': '&#xe91f;',
		'icon-planesium': '&#xe91e;',
		'icon-sign-out': '&#xf08b;',
		'icon-level-up': '&#xf148;',
		'icon-google': '&#xe91c;',
		'icon-google-plus3': '&#xe91d;',
		'icon-image': '&#xe904;',
		'icon-music': '&#xe905;',
		'icon-book': '&#xe906;',
		'icon-location': '&#xe907;',
		'icon-display': '&#xe908;',
		'icon-laptop': '&#xe909;',
		'icon-mobile': '&#xe90a;',
		'icon-mobile2': '&#xe90b;',
		'icon-tablet': '&#xe90c;',
		'icon-equalizer': '&#xe90e;',
		'icon-cog': '&#xe90f;',
		'icon-mug': '&#xe910;',
		'icon-clipboard': '&#xe911;',
		'icon-earth': '&#xe912;',
		'icon-embed2': '&#xe913;',
		'icon-embed22': '&#xe914;',
		'icon-terminal': '&#xe915;',
		'icon-xing2': '&#xe916;',
		'icon-radio-checked': '&#xea55;',
		'icon-radio-unchecked': '&#xea56;',
		'icon-github5': '&#xeab5;',
		'icon-joomla': '&#xeab8;',
		'icon-git': '&#xeae2;',
		'icon-pencil': '&#xe601;',
		'icon-disk': '&#xe602;',
		'icon-newtab': '&#xe600;',
		'icon-embed': '&#xe603;',
		'icon-code': '&#xe604;',
		'icon-console': '&#xe605;',
		'icon-share': '&#xe606;',
		'icon-twitter': '&#xe607;',
		'icon-feed': '&#xe608;',
		'icon-wordpress': '&#xe609;',
		'icon-android': '&#xe60a;',
		'icon-windows8': '&#xe60b;',
		'icon-html5': '&#xe60c;',
		'icon-css3': '&#xe60d;',
		'icon-chrome': '&#xe60e;',
		'icon-in-alt': '&#xe60f;',
		'icon-heart': '&#xe618;',
		'icon-spades': '&#xe90d;',
		'icon-js': '&#xe91a;',
		'icon-java': '&#xe91b;',
		'icon-yl-black': '&#xe917;',
		'icon-php': '&#xe918;',
		'icon-bitbucket': '&#xe903;',
		'icon-neumeister': '&#xe902;',
		'icon-kraft': '&#xe901;',
		'icon-letsencrypt-logo': '&#xe900;',
		'icon-schreg': '&#xe614;',
		'icon-yl': '&#xe615;',
		'icon-nz': '&#xe617;',
		'icon-error': '&#xe000;',
		'icon-error-outline': '&#xe001;',
		'icon-warning': '&#xe002;',
		'icon-email': '&#xe0be;',
		'icon-location-on': '&#xe0c8;',
		'icon-gps-fixed': '&#xe1b3;',
		'icon-gps-not-fixed': '&#xe1b4;',
		'icon-tv': '&#xe333;',
		'icon-blur-off': '&#xe3a4;',
		'icon-blur-on': '&#xe3a5;',
		'icon-brightness': '&#xe3ab;',
		'icon-compare': '&#xe3b9;',
		'icon-touch-app': '&#xe919;',
		'icon-cc': '&#xe610;',
		'icon-cc-by': '&#xe611;',
		'icon-cc-nc': '&#xe612;',
		'icon-cc-nd': '&#xe613;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
