<?php
namespace us\ozan\egkefsd\Repository;

use \PDO;
use us\ozan\egkefsd\Entity\Ausrede;
use \us\ozan\Settings\OzansSettings;

class AusredeRepository
{
    private $connection;

    public function __construct(PDO $connection = null)
    {
        $this->connection = $connection;
        if ($this->connection === null) {
            $Settings = new OzansSettings();
            $this->connection = new PDO(
                $Settings->getDbDsn("mysql"),
                $Settings->getDbUser(),
                $Settings->getDbPassword()
            );
            $this->connection->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );
        }
    }
    public function find($id)
    {
        $stmt = $this->connection->prepare('
            SELECT "inhalt", ausreden.*
             FROM ausreden
             WHERE id = :id
        ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Ausrede');
        return $stmt->fetch();
    }
    public function findAll()
    {
        $stmt = $this->connection->prepare('
            SELECT * FROM ausreden
        ');
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Ausrede');

        return $stmt->fetchAll();
    }
    public function save(Ausrede $ausrede)
    {
        // If the ID is set, we're updating an existing record
        if (isset($ausrede->id)) {
            return $this->update($ausrede);
        }
        $stmt = $this->connection->prepare('
            INSERT INTO ausreden
                (date_created, inhalt)
            VALUES
                (:date_created, :inhalt)
        ');
        $stmt->bindParam(':date_created', $ausrede->getDateCreated());
        $stmt->bindParam(':inhalt', $ausrede->getInhalt());
        return $stmt->execute();
    }
    public function update(Ausrede $ausrede)
    {
        if ($ausrede->getId() == null || $ausrede->getId() == "") {
            throw new \LogicException(
                'Konnte die Ausrede nicht updaten, da anscheinend diese Ausrede gar nicht existiert.'
            );
        }
        $stmt = $this->connection->prepare('
            UPDATE ausreden
            SET date_created = :date_created,
                inhalt = :inhalt,
            WHERE id = :id
        ');
        $stmt->bindParam(':date_created', $ausrede->getDateCreated());
        $stmt->bindParam(':inhalt', $ausrede->getInhalt());
        $stmt->bindParam(':id', $ausrede->getId());
        return $stmt->execute();
    }
    public function countAll()
    {
        $stmt = $this->connection->prepare('
            SELECT * FROM ausreden
        ');
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Ausrede');

        return $stmt->rowCount();
    }
}