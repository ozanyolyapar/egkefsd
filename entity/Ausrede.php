<?php
/**
 * Created by PhpStorm.
 * User: info
 * Date: 11.04.2016
 * Time: 14:49
 */

namespace us\ozan\egkefsd\Entity;

class Ausrede
{
    public $id;
    public $date_created;
    public $inhalt;

    public function __construct($data = null)
    {
        if (is_array($data)) {
            if (isset($data['id'])) $this->id = $data['id'];

            $this->id = $data['id'];
            if (isset($data['date_created'])) $this->date_created = $data['date_created'];
            else $this->date_created = date('Y-m-d G:i:s');
            $this->inhalt = $data['inhalt'];
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param string $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getInhalt()
    {
        return $this->inhalt;
    }

    /**
     * @param mixed $inhalt
     */
    public function setInhalt($inhalt)
    {
        $this->inhalt = $inhalt;
    }
}