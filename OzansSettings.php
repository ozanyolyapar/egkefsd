<?php
namespace us\ozan\Settings;

class OzansSettings
{
    private $db = array(
        "host"=>"localhost",
        "user"=>"ozans",
        "pw"=>"123",
        "name"=>"egkefsd"
    );

    /**
     * @return string
     */
    public function getDbHost(){
        return $this->db['host'];
    }

    /**
     * @return string
     */
    public function getDbUser(){
        return $this->db['user'];
    }

    /**
     * @return string
     */
    public function getDbPassword(){
        return $this->db['pw'];
    }

    /**
     * @return string
     */
    public function getDbName(){
        return $this->db['name'];
    }

    /**
     * @param string $scheme e.g. mysql
     * @return string
     */
    public function getDbDsn($scheme){
        return $scheme.":"."host=".$this->db['host'].";"."dbname=".$this->db['name'];
    }
}