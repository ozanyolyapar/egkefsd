<?php
require_once "OzansSettings.php";
require_once "entity".DIRECTORY_SEPARATOR."Ausrede.php";
require_once "repository".DIRECTORY_SEPARATOR."AusredeRepository.php";
require_once "vendor".DIRECTORY_SEPARATOR."autoload.php";
$ausredeRepository = new \us\ozan\egkefsd\Repository\AusredeRepository();
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$teile = explode("/", $path);
/**
 * @var int $id
 */
$id = intval($teile[1]);
if($id == 0){
    $id = rand(0, ($ausredeRepository->countAll())-1);
}
//var_dump($id);
//var_dump($ausredeRepository->countAll());
$data = array(
    "id"=> $id,
    "ausrede"=>$ausredeRepository->find($id),
);
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array());
$template = $twig->loadTemplate('index.twig');
echo $template->render($data);
?>